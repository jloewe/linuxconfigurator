#!/bin/bash

LC_REPO_URL=https://gitlab.com/jloewe/linuxconfigurator.git

if hash git 2>/dev/null; then
  echo "Preparing for Installation..."
  sudo rm -rf /etc/linuxconfigurator
  echo "Cloning LinuxConfigurator..."
  sudo git clone $LC_REPO_URL /etc/linuxconfigurator --branch master
  echo "Adding command 'lconf' to sbin..."
  sudo ln -sf /etc/linuxconfigurator/src/lconf /usr/sbin/lconf
else
  echo "Git is not installed. Please install it."
  echo "sudo apt-get install git"
fi
