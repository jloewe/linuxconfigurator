#!/usr/bin/env bash

source ${SCRIPTDIR}/lib/logger/colors.sh

function info() {
    echo -e "${BLUE}${BOLD}[INFO]${SET} $1"
}

function warn() {
    echo -e "${LIGHTRED}${BOLD}[WARN]${SET} $1"
}

function err() {
    echo -e "${RED}${BOLD}[ERR]${SET} $1"
}