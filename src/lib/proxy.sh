#!/usr/bin/env bash

function setproxy() {
    info "Proxy Script by jloewe"
    info "----------------------"
    info "Setting environment proxy variables."

    if [ -z ${1+x} ] || [ -z ${2+x} ]; then
        err "Usage: setproxy <proxy_host> <proxy_port> <proxy_bypass (optional)>"
        return 1
    fi

    local proxy_host=$1
    local proxy_port=$2
    local proxy_bypass=$3

    if [ -z ${proxy_bypass+x} ]; then
        warn "Proxy Bypass not set to a value"
    fi
    local proxy_bypass_comma="${proxy_bypass// /,}"

    export http_proxy="http://$proxy_host:$proxy_port/"
    export https_proxy=${http_proxy}
    export ftp_proxy=${http_proxy}
    export no_proxy=${proxy_bypass_comma}

    if ! grep -qiE "(http_proxy|https_proxy|ftp_proxy|no_proxy)" /etc/environment; then
      echo "http_proxy=$http_proxy" | sudo tee -a /etc/environment > /dev/null
      echo "https_proxy=$https_proxy"  | sudo tee -a /etc/environment > /dev/null
      echo "ftp_proxy=$ftp_proxy"  | sudo tee -a /etc/environment > /dev/null
      echo "no_proxy=$proxy_bypass_comma"  | sudo tee -a /etc/environment > /dev/null
    else
      warn "/etc/environment proxy already configured. NOT OVERWRITING."
    fi

    if ! grep -qiE "(http_proxy|https_proxy|ftp_proxy|no_proxy)" /etc/bashrc; then
      echo "export http_proxy=$http_proxy" | sudo tee -a /etc/bashrc > /dev/null
      echo "export https_proxy=$https_proxy"  | sudo tee -a /etc/bashrc > /dev/null
      echo "export ftp_proxy=$ftp_proxy"  | sudo tee -a /etc/bashrc > /dev/null
      echo "export no_proxy=$proxy_bypass_comma"  | sudo tee -a /etc/bashrc > /dev/null
    else
      warn "/etc/bashrc proxy already configured. NOT OVERWRITING."
    fi

    if ! sudo grep -qiE "(http_proxy|https_proxy|ftp_proxy|no_proxy)" /etc/sudoers; then
      info "Adding proxy vars to env_keep => /etc/sudoers"
      echo "Defaults env_keep += \"http_proxy https_proxy ftp_proxy no_proxy\"" | sudo tee -a /etc/sudoers > /dev/null
    else
      warn "env_keep => /etc/sudoers already configured. NOT OVERWRITING."
    fi

    if hash apt-get 2>/dev/null; then
        info "Setting Proxy for apt."
        if ! grep -qiE "(http::proxy|https::proxy|ftp::proxy)" /etc/apt/apt.conf; then
          echo "Acquire::http::proxy \"$http_proxy\";" | sudo tee -a /etc/apt/apt.conf > /dev/null
          echo "Acquire::https::proxy \"$https_proxy\";" | sudo tee -a /etc/apt/apt.conf > /dev/null
          echo "Acquire::ftp::proxy \"$ftp_proxy\";" | sudo tee -a /etc/apt/apt.conf > /dev/null
        else
          warn "apt proxy already configured. NOT OVERWRITING."
        fi
    else
        warn "apt is not installed."
    fi

    if hash git 2>/dev/null; then
        info "Setting Proxy for Git."
        git config --global http.proxy ${http_proxy}
        git config --global https.proxy ${https_proxy}
    else
        warn "Git is not installed."
    fi

    if hash npm 2>/dev/null; then
        info "Setting Proxy for NPM."
        npm config set proxy ${http_proxy} 1>/dev/null
        npm config set https-proxy ${https_proxy} 1>/dev/null
    else
        warn "NPM is not installed."
    fi

    if hash yarn 2>/dev/null; then
        info "Setting Proxy for Yarn."
        yarn config set proxy ${http_proxy} 1>/dev/null
        yarn config set https-proxy ${https_proxy} 1>/dev/null
    else
        warn "Yarn is not installed."
    fi

    if hash networksetup 2>/dev/null; then
        info "Setting Proxy for Mac."

        # activate proxy for wi-fi
        if ! [ -z ${proxy_bypass+x} ]; then
            sudo networksetup -setproxybypassdomains wi-fi ${proxy_bypass}
        fi
        sudo networksetup -setwebproxystate wi-fi on
        sudo networksetup -setwebproxy wi-fi ${proxy_host} ${proxy_port} off
        sudo networksetup -setsecurewebproxystate wi-fi on
        sudo networksetup -setsecurewebproxy wi-fi ${proxy_host} ${proxy_port} off
        sudo networksetup -setftpproxystate wi-fi on
        sudo networksetup -setftpproxy wi-fi ${proxy_host} ${proxy_port} off

    else
        info "Device is not Mac."
    fi

    info "----------------------"
    info "Done."
}

function unsetproxy() {
    info "Proxy Script by jloewe"
    info "----------------------"
    info "Unsetting environment proxy variables."
    unset http_proxy https_proxy ftp_proxy

    sudo grep -viE "(http_proxy|https_proxy|ftp_proxy|no_proxy)" /etc/environment | sudo tee /etc/environment.tmp > /dev/null
    sudo mv /etc/environment.tmp /etc/environment

    sudo grep -viE "(http_proxy|https_proxy|ftp_proxy|no_proxy)" /etc/bashrc | sudo tee /etc/bashrc.tmp > /dev/null
    sudo mv /etc/bashrc.tmp /etc/bashrc

    if hash apt-get 2>/dev/null; then
        info "Unsetting Proxy for apt."
        sudo grep -viE "(http::proxy|https::proxy|ftp::proxy)" /etc/apt/apt.conf | sudo tee /etc/apt/apt.conf.tmp > /dev/null
        sudo mv /etc/apt/apt.conf.tmp /etc/apt/apt.conf
    else
        warn "apt is not installed."
    fi

    if hash git 2>/dev/null; then
        info "Unsetting Proxy for Git."
        git config --global --unset http.proxy
        git config --global --unset https.proxy
    else
        warn "Git is not installed."
    fi

    if hash npm 2>/dev/null; then
        info "Unsetting Proxy for NPM."
        npm config delete proxy
        npm config delete https-proxy
    else
        warn "NPM is not installed."
    fi

    if hash yarn 2>/dev/null; then
        info "Unsetting Proxy for Yarn."
        yarn config delete proxy &> /dev/null
        yarn config delete https-proxy &> /dev/null
    else
        warn "Yarn is not installed."
    fi

    if hash yarn 2>/dev/null; then
        info "Setting Proxy for Mac."

        # deactivate proxy for wi-fi
        sudo networksetup -setwebproxystate wi-fi off
        sudo networksetup -setsecurewebproxystate wi-fi off
        sudo networksetup -setftpproxystate wi-fi off
    else
        info "Device is not Mac."
    fi

    info "----------------------"
    info "Done."
}
