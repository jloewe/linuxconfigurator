#!/usr/bin/env bash

function form() {
    if command -v whiptail &>/dev/null; then
        whiptail "$@"
    elif command -v dialog &>/dev/null; then
        dialog "$@"
    fi
}