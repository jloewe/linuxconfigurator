#!/usr/bin/env bash

function setTimezone() {
    form --msgbox LALALA 20 60
    if hash dpkg-reconfigure 2>/dev/null; then
        local timezone=${1}
        echo "${1}" | sudo tee /etc/timezone
        sudo ln -fs "/usr/share/zoneinfo/${timezone}" /etc/localtime
        sudo dpkg-reconfigure -f noninteractive tzdata
    else
        warn "Can't set timezone."
    fi
}
