#!/usr/bin/env bash

calc_wt_size() {
    # NOTE: it's tempting to redirect stderr to /dev/null, so supress error
    # output from tput. However in this case, tput detects neither stdout or
    # stderr is a tty and so only gives default 80, 24 values
    WT_HEIGHT=17
    WT_WIDTH=$(tput cols)


    if [ -z "$WT_WIDTH" ] || [ "$WT_WIDTH" -lt 60 ]; then
      WT_WIDTH=80
    fi

    if [ "$WT_WIDTH" -gt 178 ]; then
      WT_WIDTH=120
    fi

    WT_MENU_HEIGHT=$(($WT_HEIGHT-7))
}