#!/usr/bin/env bash

function do_install_updates() {
    if hash apt 2> /dev/null; then
        {
            echo -e "XXX\n0\nUpdates werden geladen...\nXXX"
            sudo apt-get update
            echo -e "XXX\n50\nUpdates werden installiert...\nXXX"
            sudo apt-get upgrade -y
            echo -e "XXX\n100\nUpdates installiert... Fertig.\nXXX"
        } | form --title "Updates installieren" --gauge "Updates werden installiert" 6 60 0
        RET=$?
        if [ ${RET} -eq 0 ]; then
            form --msgbox "Updates erfolgreich installiert" 20 60
        else
            form --title "Fehler" --msgbox "Updates konnten nicht installiert werden.\n\n\nHaben Sie die richtigen Proxyeinstellungen?" 20 60
        fi
        return 0
    else
        form --title "Fehler" --msgbox "apt ist auf Ihrem Computer nicht installiert." 20 60
    fi
}