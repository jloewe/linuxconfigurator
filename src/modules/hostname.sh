#!/usr/bin/env bash

do_change_hostname() {
    form --msgbox "\
Der Hostname darf nur die ASCII-Buchstaben 'a' bis 'z' enthalten (Groß-/Kleinschreibung wird nicht berücksichtigt)
und die Ziffern '0' bis '9' und Bindestriche enthalten.
Hostnamen können nicht mit einem Bindestrich beginnen oder enden.
Andere Symbole, Satzzeichen oder Leerzeichen sind nicht zulässig.

ACHTUNG:
Die Hostnamenänderung wird erst nach einem Neustart durchgeführt!\
" 20 70
    CURRENT_HOSTNAME=`cat /etc/hostname | tr -d " \t\n\r"`
    if [ ! -z ${CURRENT_HOSTNAME+x} ]; then
        NEW_HOSTNAME=$(form --inputbox "Please enter a hostname" 20 60 "$CURRENT_HOSTNAME" 3>&1 1>&2 2>&3)
        if [ $? -eq 0 ]; then
            sed -i 's/preserve_hostname: false/preserve_hostname: true/g' /etc/cloud/cloud.cfg
            echo ${NEW_HOSTNAME} > /etc/hostname
            sed -i "s/127.0.1.1.*$CURRENT_HOSTNAME/127.0.1.1\t$NEW_HOSTNAME/g" /etc/hosts
            ASK_TO_REBOOT=1
        fi
        return 0
    else
        form --title "Fehler" --msgbox "Der Hostname kann nicht gesetzt werden." 0 0
    fi
}
