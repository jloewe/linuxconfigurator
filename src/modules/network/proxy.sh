#!/usr/bin/env bash

source ${SCRIPTDIR}/lib/proxy.sh

function unset_proxy() {
    unsetproxy 2> /dev/null
    form --msgbox "Proxyeinstellungen zurückgesetzt" 20 60
}

function set_ptb_proxy() {
    unsetproxy 2> /dev/null
    setproxy webproxy.bs.ptb.de 8080 "localhost 127.0.0.1 idmplus.ptb.de intranet.ptb.de 192.168.71.0/24 *.ptb.de 141.25.67.0/24" 2> /dev/null
    form --msgbox "PTB-Proxy eingestellt" 20 60
}

function set_custom_proxy() {
    local proxy_host=$(form --inputbox "Bitte geben Sie den Proxy-Host OHNE PORT ein" 20 60 "proxy.example.com" 3>&1 1>&2 2>&3)
    if [ $? -eq 0 ]; then
        local proxy_port=$(form --inputbox "Bitte geben Sie den Proxy-Port ein" 20 60 "8080" 3>&1 1>&2 2>&3)
        if [ $? -eq 0 ]; then
            local proxy_bypass=$(form --inputbox "Bitte geben Sie die Proxy-Ausnahmen ein (127.0.0.1 localhost example.com usw.)" 20 60 "" 3>&1 1>&2 2>&3)
            if [ $? -eq 0 ]; then
                unsetproxy 2> /dev/null
                setproxy "${proxy_host}" "${proxy_port}" "${proxy_bypass}" 2> /dev/null
                form --msgbox "Benutzerdefinierter Proxy eingestellt" 20 60
            fi
        fi
    fi
}

function do_proxy_settings_menu() {
    FUN=$(form --title "Proxyeinstellungen" --menu "Bitte wählen Sie eine Proxyeinstellung" ${WT_HEIGHT} ${WT_WIDTH} ${WT_MENU_HEIGHT} \
    "1 Keiner" "Setzt alle Proxyeinstellungen zurück" \
    "2 PTB" "Stellt den PTB-Proxy ein" \
    "3 Benutzerdefiniert" "Stellt einen benutzerdefinierten Proxy ein" \
    3>&1 1>&2 2>&3)
    RET=$?
    if [ ${RET} -eq 1 ]; then
      return 0
    elif [ ${RET} -eq 0 ]; then
      case "$FUN" in
        1\ *) unset_proxy ;;
        2\ *) set_ptb_proxy ;;
        3\ *) set_custom_proxy ;;
        *) form --msgbox "Programmer error: unrecognized option" 20 60 ;;
      esac || form --msgbox "There was an error running option $FUN" 20 60
    else
      exit 1
    fi
}