#!/usr/bin/env bash

source ${SCRIPTDIR}/modules/network/proxy.sh

enable_ssh() {
    {
        dpkg -s "openssh-server" 2> /dev/null
        RET=$?
        if [ ${RET} -eq 1 ]; then
            echo -e "XXX\n5\nOpenSSH Server wird installiert... \nXXX"
            apt-get install openssh-server -y
        fi
        echo -e "XXX\n90\nSSH Service neustarten...\nXXX"
        systemctl unmask ssh
        systemctl enable ssh 2> /dev/null
        systemctl start ssh
        echo -e "XXX\n95\nFirewall einstellen...\nXXX"
        ufw allow ssh 2> /dev/null

    } | form --title "SSH aktivieren" --gauge "SSH wird aktiviert..." 6 60 0
    form --msgbox "SSH wurde erfolgreich aktiviert." 20 60
}

disable_ssh() {
    systemctl stop ssh
    systemctl disable ssh 2> /dev/null
    ufw deny ssh 2> /dev/null
    form --msgbox "SSH wurde erfolgreich deaktiviert." 20 60
}

do_ssh() {
    ACTIVE=$(systemctl is-active ssh)
    form --yes-button "SSH aktivieren" --no-button "SSH deaktivieren" --yesno "Möchten Sie SSH aktivieren oder deaktivieren?\n\n\nStatus: ${ACTIVE}" 20 60 2
    RET=$?
    if [ ${RET} -eq 0 ]; then
        enable_ssh
    else
        disable_ssh
    fi
}

function do_network_menu() {
    FUN=$(form --title "Netzwerkeinstellungen" --menu "Netzwerkeinstellungen ändern" ${WT_HEIGHT} ${WT_WIDTH} ${WT_MENU_HEIGHT} \
    "1 Proxy" "Ändert die Proxyeinstellungen des Systems" \
    "2 SSH" "Aktivieren des Services für SSH" \
    3>&1 1>&2 2>&3)
    RET=$?
    if [ ${RET} -eq 1 ]; then
      return 0
    elif [ ${RET} -eq 0 ]; then
      case "$FUN" in
        1\ *) do_proxy_settings_menu ;;
        2\ *) do_ssh ;;
        *) form --msgbox "Programmer error: unrecognized option" 20 60 ;;
      esac || form --msgbox "There was an error running option $FUN" 20 60
    else
      exit 1
    fi
}