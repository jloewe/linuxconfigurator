#!/usr/bin/env bash

function change_current_user_password() {
    new_password=$(form --title "Passwort ändern" --passwordbox "Bitte geben Sie das neue Passwort für ${SUDO_USER} ein." 8 78 3>&1 1>&2 2>&3)
    if [ $? -eq 0 ]; then
        new_password2=$(form --title "Passwort bestätigen" --passwordbox "Bitte bestätigen Sie das neue Passwort für ${SUDO_USER}." 8 78 3>&1 1>&2 2>&3)
        if [ $? -eq 0 ]; then
            if [ ${#new_password} -ge 6 ]; then
                if [ ${#new_password} -eq ${#new_password2} ]; then
                    echo -e "${new_password}\n${new_password2}" | passwd ${SUDO_USER} 2> /dev/null
                    form --msgbox "Password für ${SUDO_USER} erfolgreich geändert." 20 60
                    return 0
                else
                    form --title "Fehler" --msgbox "Die Passwörter stimmen nicht überein." 20 60
                fi
            else
                form --title "Fehler" --msgbox "Die Passwort muss mindestens 6 Zeichen lang sein." 20 60
            fi
        fi
    fi
}

function create_new_user() {
    new_username=$(form --title "Benutzernamen festlegen" --inputbox "Bitte geben Sie den Benutzernamen ein." 8 78 3>&1 1>&2 2>&3)
    if [ $? -eq 0 ]; then
        new_password=$(form --title "Passwort festlegen" --passwordbox "Bitte geben Sie das Passwort für ${new_username} ein." 8 78 3>&1 1>&2 2>&3)
        if [ $? -eq 0 ]; then
            new_password2=$(form --title "Passwort bestätigen" --passwordbox "Bitte bestätigen Sie das Passwort für ${new_username}." 8 78 3>&1 1>&2 2>&3)
            if [ $? -eq 0 ]; then
                if [ ${#new_password} -ge 6 ]; then
                    if [ ${#new_password} -eq ${#new_password2} ]; then
                        adduser --quiet --disabled-password --gecos "" "${new_username}"
                        echo -e "${new_password}\n${new_password2}" | passwd ${new_username} 2> /dev/null
                        form --msgbox "Benutzer ${new_username} erfolgreich angelegt." 20 60
                        form --defaultno --yesno "Soll der Benutzer ${new_username} Root-Rechte bekommen?" 20 60 2
                        RET=$?
                        if [ ${RET} -eq 0 ]; then
                            usermod -aG sudo "${new_username}"
                            form --msgbox "Der Benutzer ${new_username} hat nun Root-Rechte." 20 60
                        fi
                        return 0
                    else
                        form --title "Fehler" --msgbox "Die Passwörter stimmen nicht überein." 20 60
                    fi
                else
                    form --title "Fehler" --msgbox "Die Passwort muss mindestens 6 Zeichen lang sein." 20 60
                fi
            fi
        fi
    fi
}

function do_user_menu() {
    FUN=$(form --title "Benutzereinstellungen" --menu "Bitte wählen Sie einen Menüpunkt" ${WT_HEIGHT} ${WT_WIDTH} ${WT_MENU_HEIGHT} \
    "1 Passwort" "Passwort für ${SUDO_USER} setzen" \
    "2 Benutzer" "Neuen Benutzer erstellen" \
    3>&1 1>&2 2>&3)
    RET=$?
    if [ ${RET} -eq 1 ]; then
      return 0
    elif [ ${RET} -eq 0 ]; then
      case "$FUN" in
        1\ *) change_current_user_password ;;
        2\ *) create_new_user ;;
        *) form --msgbox "Programmer error: unrecognized option" 20 60 ;;
      esac || form --msgbox "There was an error running option $FUN" 20 60
    else
      exit 1
    fi
}