#!/usr/bin/env bash

function do_change_locale() {
    dpkg-reconfigure locales
}

function do_change_timezone() {
    dpkg-reconfigure tzdata
}

function do_german_defaults() {
    {
        echo -e "XXX\n0\nSprache wird generiert... \nXXX"
        # locale
        locale-gen de_DE.UTF-8
        echo -e "XXX\n20\nSprache wird eingestellt... \nXXX"
        update-locale LANG=de_DE.UTF-8

        # timezone
        echo -e "XXX\n40\nZeitzoneneinstellungen werden zurückgesetzt... \nXXX"
        rm /etc/localtime
        echo -e "XXX\n60\nZeitzone wird auf Berlin gesetzt... \nXXX"
        echo "Europe/Berlin" > /etc/timezone
        echo -e "XXX\n80\nZeitzone wird übernommen... \nXXX"
        dpkg-reconfigure -f noninteractive tzdata &> /dev/null
        echo -e "XXX\n100\nZeitzone eingestellt... Fertig.\nXXX"
    } | form --title "Standardeinstellungen setzen" --gauge "Standardeinstellungen für Deutschland werden gesetzt" 6 60 0

    form --msgbox "Standardeinstellungen für Deutschland gesetzt" 20 60
}

function do_internationalisation_menu() {
    FUN=$(form --title "Spracheinstellungen" --menu "Bitte wählen Sie eine Proxyeinstellung" ${WT_HEIGHT} ${WT_WIDTH} ${WT_MENU_HEIGHT} \
    "I1 Sprache" "Sprache anpassen" \
    "I2 Zeitzone" "Zeitzone anpassen" \
    "I3 Standardeinstellungen" "Alle Einstellungen für Deutschland setzen" \
    3>&1 1>&2 2>&3)
    RET=$?
    if [ ${RET} -eq 1 ]; then
      return 0
    elif [ ${RET} -eq 0 ]; then
      case "$FUN" in
        I1\ *) do_change_locale ;;
        I2\ *) do_change_timezone ;;
        I3\ *) do_german_defaults ;;
        *) form --msgbox "Programmer error: unrecognized option" 20 60 ;;
      esac || form --msgbox "There was an error running option $FUN" 20 60
    else
      exit 1
    fi
}