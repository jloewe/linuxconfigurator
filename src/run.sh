#!/usr/bin/env bash

SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

ASK_TO_REBOOT=0

# load logger
source lib/logger/logger.sh
source lib/form.sh

if [[ $UID != 0 ]]; then
    form --title "Fehler" --msgbox "Bitte führen Sie das Skript mit sudo aus." 20 60
    exit 1
fi

source lib/util.sh

source modules/network/network.sh
source modules/hostname.sh
source modules/updates.sh
source modules/internationalisation.sh
source modules/user.sh

calc_wt_size


do_finish() {
  if [ ${ASK_TO_REBOOT} -eq 1 ]; then
    form --yesno "Wollen Sie einen Neustart durchführen?" 20 60
    if [ $? -eq 0 ]; then # yes
      sync
      reboot
    fi
  fi
  exit 0
}

while true; do
  FUN=$(form --title "LinuxConfigurator von jloewe" --menu "Einstellungsmöglichkeiten" ${WT_HEIGHT} ${WT_WIDTH} ${WT_MENU_HEIGHT} \
    "1 Netzwerk" "Ändert die Netzwerkeinstellungen des Systems" \
    "2 Hostname" "Ändert den Hostname des Computers" \
    "3 Updates" "Installiert Updates" \
    "4 Sprache" "Sprache und Zeitzone einstellen" \
    "5 Benutzer" "Passwörter ändern und Benutzer erstellen" \
    3>&1 1>&2 2>&3)
  RET=$?
  if [ ${RET} -eq 1 ]; then
    do_finish
  elif [ ${RET} -eq 0 ]; then
    case "$FUN" in
      1\ *) do_network_menu ;;
      2\ *) do_change_hostname ;;
      3\ *) do_install_updates ;;
      4\ *) do_internationalisation_menu ;;
      5\ *) do_user_menu ;;
      *) form --msgbox "Programmer error: unrecognized option" 20 60 ;;
    esac || form --msgbox "There was an error running option $FUN" 20 60
  else
    exit 1
  fi
done
