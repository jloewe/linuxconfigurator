# LinuxConfigurator

A utility for configuring linux systems like raspi-config

## Installation

```sh
wget -O - https://gitlab.com/jloewe/linuxconfigurator/raw/master/install.sh | bash
```

:zap: OR

```sh
curl https://gitlab.com/jloewe/linuxconfigurator/raw/master/install.sh | bash
```

## Usage

```sh
lconf
```
